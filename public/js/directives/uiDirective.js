var moduleDirective =  angular.module("uiDirective", []);

moduleDirective.directive("uiForm", function(vPath, utilValid) {
    return {
        restrict : "A",
        scope : {
            action : "@action",
            name : "@name",
            notSubmit : "@notSubmit"
        },
        controller : function($scope, $element, $attrs) {
            var scopes = [];
            if ($scope.notSubmit) {
                $element.on('submit', function() {
                    return false;
                });
            }
            var inputs = [];
            this.addScope = function(scope) {
                inputs.push(scope);
            };
            this.isFormValid = function() {
                return inputs.some(function(scope) {
                    return (scope.notEmpty && !scope.value) || (scope.showErro);
                });
            };
            function isValid() {
                return scopes.some(function(scope) {
                    if(scope.isSelect){
                        return ($.isEmptyObject(scope.value) && $.isEmptyObject(scope.multValue));
                    }else{
                        return (scope.notEmpty && !scope.value)	|| (scope.showErro) ;
                    }
                });
            }
            function setSubmit() {
                $element.on('submit', function() {
                    return !isValid();
                });
            }
            this.setValidForm = function() {
                setSubmit();
            };
            this.addScope = function(scope) {
                scopes.push(scope);
            };
            this.isFormValid = function() {
                return isValid();
            };
            this.showListMsg = function() {
                utilValid.validScopes(scopes);
            };
        }
    };
});

moduleDirective.directive("uiInput", function(utilListener, vPath, utilEvent, $timeout) {
    return {
        templateUrl : vPath.VIEWS + "ui-input.html",
        replace : true,
        scope : {
            name : "@name",
            width : "@width",
            addClass : "@addClass",
            label : "@label",
            type : "@type",
            iconBefore : "@iconBefore",
            iconAfter : "@iconAfter",
            placeholder : "@placeholder",
            notEmpty : "@notEmpty",
            datePicker : "@datePicker",
            maskDate : "@maskDate",
            validDate : "@validDate",
            clockPicker : "@clockPicker",
            maskClock : "@maskClock",
            validClock : "@validClock",
            maskCpf : "@maskCpf",
            validCpf : "@validCpf",
            maskCnpj : "@maskCnpj",
            validCnpj : "@validCnpj",
            maskCep : "@maskCep",
            validCep : "@validCep",
            validEmail : "@validEmail",
            validNumber : "@validNumber",
            maskNumber : "@maskNumber",
            maskMax : "@maskMax",
            countMax: "@countMax",
            validMin : "@validMin",
            popover : "@popover",
            compare : "=compare",
            compareLabel : "@compareLabel",
            value : "=value"
        },
        require : "^?uiForm",
        link : function(scope, element, attrs, ctrl) {
            if (!scope.width) {
                scope.width = 12;
            }
            if(!scope.type){
                type = "text";
            }
            if (ctrl) {
                ctrl.addScope(scope);
            }
            scope.onBlur = function() {
                utilEvent.onBlurInputFloat(scope, element);
            }
            utilListener.setListenerInputFloat(scope, element);

            if (!scope.label) {
                scope.notFloat = "not-floating";
            }
            if(scope.popover){
                $timeout(function(){
                    element.popover({content: scope.popover, title: scope.label || scope.placeholder, trigger: "focus"});
                },500);
            }
        }
    };
});

moduleDirective.directive("uiTextarea", function(utilListener, vPath, utilEvent, $timeout) {
    return {
        templateUrl : vPath.VIEWS + "ui-textarea.html",
        replace : true,
        scope : {
            name : "@name",
            width : "@width",
            addClass : "@addClass",
            label : "@label",
            rows : "@rows",
            iconBefore : "@iconBefore",
            iconAfter : "@iconAfter",
            placeholder : "@placeholder",
            notEmpty : "@notEmpty",
            maskMax : "@maskMax",
            countMax: "@countMax",
            validMin : "@validMin",
            popover : "@popover",
            value : "=value"
        },
        require : "^?uiForm",
        link : function(scope, element, attrs, ctrl) {
            if (!scope.width) {
                scope.width = 12;
            }
            if (ctrl) {
                ctrl.addScope(scope);
            }
            scope.onBlur = function() {
                utilEvent.onBlurInputFloat(scope, element);
            };
            utilListener.setListenerInputFloat(scope, element);

            if (!scope.label) {
                scope.notFloat = "not-floating";
            }
            if(scope.popover){
                $timeout(function(){
                    element.popover({content: scope.popover, title: scope.label || scope.placeholder, trigger: "focus"});
                },500);
            }
        }
    };
});

moduleDirective.directive("uiPaper", function(vPath) {
    return {
        templateUrl : vPath.VIEWS + "ui-paper.html",
        replace : true,
        transclude : true,
        scope : {
            width : "@width",
            addClass : "@addClass"
        },
        link : function(scope, element, attrs) {
            if (!scope.width) {
                scope.width = 12;
            }
        }
    };
});

moduleDirective.directive("uiButton",	function(utilListener, vPath, vMsg, $timeout, utilDialog, $rootScope) {
    return {
        templateUrl : vPath.VIEWS + "ui-button.html",
        replace : true,
        transclude : true,
        scope : {
            width : "@width",
            addClass : "@addClass",
            icon : "@icon",
            type : "@type",
            link : "@link",
            tooltip : "@tooltip",
            titleTooltip : "@titleTooltip",
            triggerTooltip : "@triggerTooltip",
            float : "@float",
            badges : "=badges",
            btnDisabled : "=btnDisabled",
            showValidation : "@showValidation",
            validForm : "@validForm"
        },
        require : "^?uiForm",
        link : function(scope, element, attrs, ctrl) {
            if(!scope.triggerTooltip){
                scope.triggerTooltip = "hover";
            }
            if (scope.showValidation) {
                element.bind('click', function(e) {
                    if (ctrl && scope.showValidation && ctrl.isFormValid() && !scope.link) {
                        try {
                            e.stopImmediatePropagation();
                            return false;
                        } catch (e) {}
                    }
                });
            }
            scope.isFormValid = function() {
                if (ctrl && scope.validForm) {
                    return ctrl.isFormValid();
                } else {
                    return false;
                }
            };
            scope.showListMsg = function() {
                if (ctrl && scope.showValidation && ctrl.isFormValid() && !scope.link) {
                    ctrl.showListMsg();
                } else if (scope.link && scope.link != "#") {
                    var index = scope.link.indexOf("://") ? (scope.link.indexOf("//") + 2) : 0;
                    var url = scope.link.substring(index);
                    $timeout(function() {
                        utilDialog.openLoading(scope, vMsg.AGUARDE_CARREGANDO + url);
                    }, 500);
                }
            };
            if (ctrl && scope.showValidation) {
                ctrl.setValidForm();
            }
            var floatClass = "";
            if (scope.float) {
                if (scope.float == "left-bottom") {
                    floatClass = "float-left-bottom";
                } else if (scope.float == "right-bottom") {
                    floatClass = "float-right-bottom";
                } else if (scope.float == "right-top") {
                    floatClass = "float-right-top";
                } else {
                    floatClass = "float-left-top";
                }
            }
            $timeout(function() {
                if (scope.float) {
                    element.addClass("btn-float");
                    element.addClass(floatClass);
                }
                if (scope.badges) {
                    element.find(".btn").css("overflow", "visible")
                }
                element.find(".btn-fab").fadeIn("5000");

                $.material.ripples(element.find(".btn"));
                if (scope.tooltip) {
                    element.attr('title', scope.titleTooltip);
                    element.tooltip({
                        placement : scope.tooltip, trigger: scope.triggerTooltip,
                        delay : {
                            "show" : 300,
                            "hide" : 400
                        }
                    });
                }
            }, 100);
        }
    }
});

moduleDirective.directive("uiNavbar", function(vPath, $window, $timeout, utilDialog, utilFunc) {
    return {
        templateUrl : vPath.VIEWS + "ui-navbar.html",
        replace : true,
        transclude : true,
        scope : {
            label : "@label",
            addClass : "@addClass",
            imgLogo : "@imgLogo",
            fixed : "@fixed",
            url : "@url"
        },
        link : function(scope, element, attrs) {
            scope.img = vPath.IMG + scope.imgLogo;
            $.material.ripples(element.find("a"));
            scope.fecharSlideRigh = function(){
                utilDialog.closeSlideRight();
            }
            //Se a navbar for fixa.
            if(scope.fixed){
                utilFunc.setFixeNav(element);
                angular.element($window).bind("scroll", function(){
                    if(this.pageYOffset > 0 ){
                        utilFunc.setFixeNav(element, true);
                    }else{
                        utilFunc.setFixeNav(element, false);
                    }
                });
            }
            //Caso o slide right tenha scroll ele seja maior que zero.
            angular.element(element.find(".nav-slide-right-container")).bind("scroll", function(){
                    if(this.scrollTop > 0 ){
                        element.find(".nav-slide-right-title").addClass("shadow-4");
                    }else{
                        element.find(".nav-slide-right-title").removeClass("shadow-4");
                    }
            });
        }
    };
});

moduleDirective.directive("uiMenuDrop", function(vPath, $document) {
    return {
        templateUrl : vPath.VIEWS + "ui-menu-drop.html",
        replace : true,
        transclude : true,
        scope : {
            icon : "@icon",
            label : "@label",
            badges : "=badges",
            addClass : "@addClass"
        },
        link : function(scope, element, attrs) {
            scope.visible = false;
            var btn = element.find(".btn");
            var drop = element.find(".dropdown-menu");
            btn.on("click", function(){
                if(scope.visible){
//                    drop.css("left", "100%");
                    drop.removeClass("show-drop");
                    scope.visible = false;
                }else{
                    drop.css("left", btn.offset().left - (drop.width() - (btn.width()*1.2)));
                    drop.addClass("show-drop");
                    scope.visible = true;
                }
            });
            $document.on("click", function() {
                if (!element.is(":hover")) {
//                    drop.css("left", "100%");
                    drop.removeClass("show-drop");
                    scope.visible = false;
                }
            });
            $.material.ripples(btn);
        }
    };
});

moduleDirective.directive("uiLinkMenuRight", function(vPath, $timeout) {
    return {
        templateUrl : vPath.VIEWS + "ui-link-menu-right.html",
        replace : true,
        transclude : true,
        scope : {
            icon : "@icon",
            label : "@label",
            url : "@url"
        },
        link : function(scope, element, attrs) {
            $timeout(function(){
                $.material.ripples(element.find(".btn"));
            },300);
        }
    };
});

moduleDirective.directive("uiRadio",function(vPath, utilFunc, $timeout){
    return {
        templateUrl: vPath.VIEWS + "ui-radio.html",

        scope: {
            addClass:"@addClass",
            width: "@width",
            name: "@name",
            label: "@label",
            checked: "@checked",
            setValue: "@setValue",
            value: "=value",
            onSelected: "=onSelected"
        },
        require : "^?uiForm",
        link: function(scope, element, attrs, ctrl){
            if (ctrl) {
                ctrl.addScope(scope);
            }
            if(!scope.width){
                scope.width = 12;
            }
            if(scope.checked){
                scope.value = scope.setValue;
            }
            if(scope.onSelected){
                scope.$watch('value', function(newValue, oldValue, scope) {
                    if(scope.value == scope.setValue && newValue){
                        scope.onSelected(scope.setValue);
                    }
                });
            }
            $.material.radio(element.find("input"));
        }
    };
	
});

moduleDirective.directive("uiListcheck", function(utilListener, vPath, $timeout) {
    return {
        templateUrl : vPath.VIEWS + "ui-listcheck.html",

        scope : {
            addClass : "@addClass",
            addClassCheck : "@addClassCheck",
            width : "@width",
            widthCheck : "@widthCheck",
            label : "@label",
            checked : "@checked",
            field : "@field",
            list : "=list",
            selecteds : "=selecteds"
        },
        controller: function($scope, $element, $attrs){
            $scope.cheks = [];
            this.addCheck = function(scope){
                $scope.checks.push(scope);
            };
        },
        require : "^?uiForm",
        link : function(scope, element, attrs, ctrl) {
            if(!scope.width){
                scope.width = 12;
            }
            if(!scope.widthCheck){
                scope.widthCheck = 12;
            }
        }
    };
});

angular.module("uiDirective").directive("uiCheckbox", function(utilListener, vPath, $timeout) {
    return {
        templateUrl : vPath.VIEWS + "ui-checkbox.html",
        scope : {
            addClass : "@addClass",
            width : "@width",
            checked : "@checked",
            field : "@field",
            onChecked : "=onChecked",
            value : "=value",
            selecteds : "=selecteds"
        },
        require : "^?uiListcheck",
        link : function(scope, element, attrs, ctrl) {
            if(!scope.width){
                scope.width = 12;
            }
            scope.init = false;
            $timeout(function(){
                scope.init = true;
            }, 1000);
            scope.$watch('checked', function(newValue, oldValue, scope) {
                utilListener.setCheckbox(scope, element);
            });
            scope.onClick = function(){
                var input = element.find("input");
                var checked = input.is(":checked");
                if(scope.init && scope.onChecked && checked){
                    $timeout(function(){
                        scope.onChecked(scope.value);
                    }, 100);
                }
            };
            $.material.checkbox(element.find("input"));
        }
    };
});

moduleDirective.directive("uiToggle",	function(utilListener, vPath, $timeout) {
    return {
        templateUrl : vPath.VIEWS + "ui-toggle.html",
        replace : true,
        scope : {
            width : "@width",
            addClass : "@addClass",
            labelTrue : "@labelTrue",
            labelFalse : "@labelFalse",
            onChange : "=onChange",
            value : "=value"
        },
        link : function(scope, element, attrs, ctrl) {
            scope.flagIni = false;//Flag para não chamar a função de onChange na inicilização.
            utilListener.setListenerToggle(scope, element);
            if(!scope.width){
                scope.widit = 12;
            }
            $timeout(function(){
                scope.flagIni = true;
            },200);
        }
    }
});

moduleDirective.directive("uiMenuSlideLeft", function(vPath, $timeout) {
    return {
        templateUrl : vPath.VIEWS + "ui-menu-slide-left.html",
        replace : true,
        transclude : true,
        scope : {
            icon : "@icon",
            label : "@label"
        },
        link : function(scope, element, attrs) {
            element.find("label:first").each(function() {
                $.material.ripples(this);
            });
            $timeout(function(){
                element.find(".nav-left-container").fadeIn(1000);
            }, 1000);
        }
    }
});

moduleDirective.directive("uiLiDrop", function(vPath) {
    return {
        templateUrl : vPath.VIEWS + "ui-li-drop.html",
        replace : true,
        transclude : true,
        scope : {
            icon : "@icon",
            url : "@url"
        },
        link : function(scope, element, attrs) {
            if (!scope.url) {
                scope.url = "#";
            }
            $.material.ripples(element.find("a"));
        }
    };
});

moduleDirective.directive("uiLiMenu", function(vPath) {
    return {
        templateUrl : vPath.VIEWS + "ui-li-menu.html",
        replace : true,
        scope : {
            url : "@url",
            label : "@label",
            addClass : "@addClass",
            icon : "@icon"
        },
        link : function(scope, element, attrs) {
            if (!scope.url) {
                scope.url = "#";
            }
            $.material.ripples(element.find(".li-link-menu"));
        }
    };
});

moduleDirective.directive("uiLinkMenu", function(vPath, $timeout, utilDialog, vMsg) {
    return {
        templateUrl : vPath.VIEWS + "ui-link-menu.html",
        replace : true,
        scope : {
            url : "@url",
            label : "@label",
            addClass : "@addClass",
            icon : "@icon"
        },
        link : function(scope, element, attrs) {
            if (!scope.url) {
                scope.url = "#";
            }
            $.material.ripples(element.find("a"));
        }
    };
});

moduleDirective.directive("uiMenuLink", function(vPath) {
    return {
        templateUrl : vPath.VIEWS + "ui-menu-link.html",
        replace : true,
        transclude : true,
        scope : {
            url : "@url",
            label : "@label",
            addClass : "@addClass",
            width : "@width",
            icon : "@icon"
        },
        link : function(scope, element, attrs) {
            if (!scope.url) {
                scope.url = "#";
            }
            $.material.ripples(element.find(".ui-menu-link"));
        }
    };
});

moduleDirective.directive("uiLoadingLine", function(vPath) {
    return {
        templateUrl : vPath.VIEWS + "ui-loading-line.html",
        replace : true,
        scope : {
            addClass : "@addClass",
            width : "@width",
            show : "=show"
        },
        link : function(scope, element, attrs) {
            if (!scope.width) {
                    scope.width = 12;
            }
        }
    };
});

moduleDirective.directive("uiLoadingCircle", function(vPath, utilFunc, $timeout, $window) {
    return {
        templateUrl : vPath.VIEWS + "ui-loading-circle.html",
        replace : true,
        scope : {
            addClass : "@addClass",
            label : "@label",
            size : "@size",
            show : "=show"
        },
        link : function(scope, element, attrs) {
            if (!scope.width) {
                scope.width = 12;
            }
            angular.element($window).bind('resize', function() {
                utilFunc.configLoading(scope, element);
            });
            scope.$watch('show', function() {
                utilFunc.configLoading(scope, element);
            });
        }
    };
});

moduleDirective.directive("uiTable",function(vPath, vMsg, utilDialog, utilFunc) {
    return {
        templateUrl : vPath.VIEWS + "ui-table.html",
        replace : true,
        transclude : true,
        scope : {
            width : "@width",
            addClass : "@addClass",
            pagination : "@pagination",
            fixed : "@fixed",
            data : "=data",
            objFilter : "=objFilter",
            loading : "=loading",
            onLoad : "=onLoad",
            onEdit : "=onEdit",
            onDelete : "=onDelete"
        },
        controller : function($scope, $element, $attrs) {
            function isConstainsCollumn(f, s){//Função que recebe os filds e Scope a ser adiciona e verifica se já tem o id adicionado.
                    for(var i = 0; i < fields.length;i++){
                            if(f[i].label == s.label){
                                    return true;
                            }
                    }
                    return false;
            }
            var fields = [];
            var collumns = [];
            $scope.fields = fields;
            $scope.collumns = collumns;
            $scope.listRowPages = [{id: 5, text: "5"}, {id: 10, text: "10"}, {id: 20, text: "20"}, {id: 30, text: "30"}, {id: 40, text: "40"},
                                  {id: 50, text: "50"}, {id: 100, text: "100"}, {id: 300, text: "300"}, {id: 500, text: "500"}, {id: 1000, text: "1000"}];
            this.addField = function(scope) {
                if(!isConstainsCollumn($scope.fields, scope)){
                    $scope.collumns.push(scope);
                }
                $scope.fields.push(scope);
            };
            this.setOrder = function(order, crescente, scope, click) {
                $scope.objFilter.ordem = order;
                $scope.objFilter.crescente = crescente;

                fields.forEach(function(field) {
                    field.click = 0;
                    if(field.field == order){
                            field.click = click;
                    }
                });
                $scope.onLoad();
            };
        },
        link : function(scope, element, attrs) {
            scope.rowPages = vMsg.REGISTROS_PAGINA;
            scope.rowPageSelected = {id:5, text:"5"};
            function loadTable(){
                    scope.objFilter.qtdRegistros = scope.rowPageSelected.id;
                    scope.onLoad();
            }
            scope.$watch("data", function() {
                var data = scope.data;
                var pageInfo = "";
                var end = (data.pagina != data.qtdPaginas) ? (data.pagina * data.qtdRegistros): data.totalRegistros;
                var init = (data.pagina != data.qtdPaginas) ? (end + 1)	- data.qtdRegistros : ((data.qtdPaginas - 1) * data.qtdRegistros) + 1;
                if (data.pagina < data.qtdPaginas) {
                    scope.rightShow = false;
                    scope.lastShow = false;
                } else {
                    scope.rightShow = true;
                    scope.lastShow = true;
                }
                if (data.pagina > 1) {
                    scope.leftShow = false;
                    scope.firstShow = false;
                } else {
                    scope.leftShow = true;
                    scope.firstShow = true;
                }
                if (data.pagina	&& data.qtdPaginas && scope.pagination) {
                    pageInfo = init + "/" + end + " " + vMsg.DE;
                }
                if (data.totalRegistros) {
                    pageInfo += " "	+ data.totalRegistros + " Registro(s)";
                }
                if (!pageInfo) {
                    pageInfo = vMsg.SEM_REGISTROS;
                }
                scope.pageInfo = pageInfo;
            });
            scope.clickButton = function(button) {
                if (scope.objFilter) {
                    if (button == "first") {
                        scope.objFilter.pagina = 1;
                    }
                    if (button == "left") {
                        scope.objFilter.pagina = (scope.data.pagina - 1);
                    }
                    if (button == "right") {
                        scope.objFilter.pagina = (scope.data.pagina + 1);
                    }
                    if (button == "last") {
                        scope.objFilter.pagina = scope.data.qtdPaginas;
                    }
                }
                loadTable();
            };
            scope.changeSelect = function(item) {
                scope.rowPageSelected = item;
                loadTable();
            };
            scope.editClick = function(item) {
                scope.onEdit(item);
            };
            scope.deleteClick = function(item) {				
                scope.onDelete(item);
            };
            if(scope.fixed){//Deixando os heads fixos.
                utilFunc.setTableFixed(element);
            }
        }
    };
});

moduleDirective.directive("uiThead", function(vPath, vMsg, $compile, $timeout) {
    return {
        templateUrl : vPath.VIEWS + "ui-thead.html",
        replace : true,
        scope : {
            width : "@width",
            addClass : "@addClass",
            field : "@field",
            labelTooltipEdit : "@labelTooltipEdit",
            labelTooltipDelete : "@labelTooltipDelete",
            labelBtnEdit : "@labelBtnEdit",
            labelBtnDelete : "@labelBtnDelete",
            label: "@label",
            orderBy : "@orderBy",
            isAction : "@isAction"
        },
        require : "^?uiTable",
        link : function(scope, element, attrs, ctrl) {
            if (!scope.labelTooltipEdit) {
                scope.labelTooltipEdit = vMsg.EDITAR;
            }
            if (!scope.labelTooltipDelete) {
                scope.labelTooltipDelete = vMsg.DELETE;
            }
            if (ctrl) {
                ctrl.addField(scope);
            }
            if (scope.orderBy) {
                var icon1 = $('<i ng-if="click == 1" ng-click="setOrder(2);" class="material-icons">arrow_upward</i>');
                var icon2 = $('<i ng-if="click == 2" ng-click="setOrder(0);" class=" material-icons">arrow_downward</i>');
                var icon3 = $('<i ng-if="click == 0" ng-click="setOrder(1);" class="not-order material-icons">remove</i>');

                $.material.ripples(icon1);
                $.material.ripples(icon2);
                $.material.ripples(icon3);
                icon1 = $compile(icon1)(scope);
                icon2 = $compile(icon2)(scope);
                icon3 = $compile(icon3)(scope);
                element.append(icon1);
                element.append(icon2);
                element.append(icon3);
                scope.trOrder = 'tr-order';
                scope.click = 0;
            }
            scope.setOrder = function(click) {
                var field = scope.field, crescente = {0: true,	1: true, 2: false};
                if (ctrl) {
                    if (click == 0) {
                        field = "id";
                    }
                    ctrl.setOrder(field, crescente[click], scope, click);
                }
            };
        }
    };
});

moduleDirective.directive("uiSelect",	function(vPath, $document, utilListener, vMsg, utilValid, utilFunc, $timeout) {
    return {
        templateUrl : vPath.VIEWS + "ui-select.html",
        scope : {
            width : "@width",
            addClass : "@addClass",
            label : "@label",
            placeholder : "@placeholder",
            field : "@field",
            notEmpty : "@notEmpty",
            isFilter : "@isFilter",
            notDiselected : "@notDiselected",
            iconBefore : "@iconBefore",
            list : "=list",
            funcClick: "=funcClick",
            multValue : "=multValue",
            value : "=value"
        },
        require : "^?uiForm",
        link : function(scope, element, attrs, ctrl) {
            scope.isSelect = true;
            scope.isFirstLoad = true;//Flag para não validar na primeira vez que carrega a página.
            scope.msgFilter = vMsg.FILTRAR;
            if (ctrl) {
                ctrl.addScope(scope);
            }
            if (!scope.width) {
                scope.width = 12;
            }
            if(!scope.isFilter){
                element.find(".li-filtro-select").css("display", "none");
            }
            if (!scope.label) {
                scope.notFloat = "not-floating";
            }
            element.find(".form-control:first").on("focus", function() {
                utilFunc.setPositionSelect(element);
                element.find("#select" + scope.$id).prop("checked", true);
            });
            $document.on("click", function() {
                if (!element.find(".lista-select").is(":hover")
                        && !element.find(".form-control:first").is(":hover")) {
                    element.find("#select" + scope.$id).prop("checked", false);
                }
            });
            scope.closeSelect = function() {
                element.find("#select" + scope.$id).prop("checked", false);
            };
            scope.onClickLi = function(item) {
                utilValid.validClickSelect(scope, element, item);
                utilValid.validSelect(scope, element);
                if(scope.funcClick){
                        scope.funcClick(item);
                }
            };
            scope.finishRenderList = function(){
                if(!scope.onRender){
                    $timeout(function(){
                        utilValid.validSetSelect(scope, element, true);
                        scope.onRender = false;
                    }, 500);
                }
                scope.onRender = true;
            };
            utilListener.setListenerSelect(scope, element);
        }
    };
});

moduleDirective.directive("uiListTabs", function(vPath, $timeout, $window, $compile) {
    function getTabSelected(scope, element){
        return element.find("#tab" + scope.indexSelected);
    }
    function getMaxLeft(element){
        var containerWidth = parseInt(element.find(".tabs-container-header").css("width").replace("px", ""));
        var backgroundWidth = parseInt(element.find(".tabs-background").css("width").replace("px", ""));
        var maxLeft = containerWidth - backgroundWidth;
        return maxLeft;
    }
    function setPrevNext(element){
        var tabContainer = element.find(".tabs-container-header");
        var tabBackground = element.find(".tabs-background");
        var backgroundLeft = parseInt(tabBackground.css("left").replace("px", ""));
        var maxLeft = getMaxLeft(element);
        if(parseInt(tabBackground.css("left").replace("px", "")) > maxLeft){
            element.find(".tab-next").fadeIn(300);
            tabContainer.css("margin-right", "2em");
        }else{
            element.find(".tab-next").fadeOut(300);
            tabContainer.css("margin-right", "0em");
        }
        if(backgroundLeft < 0){
            element.find(".tab-prev").fadeIn(300);
            tabContainer.css("margin-left", "2em");
        }else{
            element.find(".tab-prev").fadeOut(300);
            tabContainer.css("margin-left", "0em");
        }
    }
    function getScopeTab(scope){
        var scopeTab = null;
        if(scope.indexSelected){
            scopeTab = scope.listTabs[scope.indexSelected];
        }else{
            scopeTab = scope.listTabs[0];
            scope.indexSelected = 0;
        }
        return scopeTab;
    }
    
    function setPositionTab(scope, element){
        var scopeTab = getScopeTab(scope);
        if(scopeTab){
            element.find(".tabs").css("color", "rgba(0, 0, 0, 0.4)");
            var tabSelected = getTabSelected(scope, element);
            var heightContainer = element.find("#tabs-body" + scopeTab.$id).css("height");
            tabSelected.css({color: "rgba(0, 0, 0, 1)"});
            element.find("#line-tab-select").css("left", tabSelected.position().left).css("width",  tabSelected.css("width"));
            element.find(".tabs-body-background").css("left", "-" + scope.indexSelected + "00%");
            element.find(".tabs-container-body").css("height", heightContainer);
            setPrevNext(element);
            if(scopeTab.page && scopeTab.pageNotLoad){
                var pageHTML = $('<div></div>');
                pageHTML.load(scopeTab.page, null, function(){
                pageHTML = $compile(pageHTML)(scope);
                element.find(".tabs-body-circle").html(pageHTML);
                scopeTab.pageNotLoad = false;
                $timeout(function(){
                    heightContainer = element.find("#tabs-body" + scopeTab.$id).css("height");
                    element.find(".tabs-container-body").css("height", heightContainer);
                    if(element.find("#line-tab-select").css("left") != tabSelected.position().left){
                        setPositionTab(scope, element);
                    }
                }, 500);
                });
            }
        }
    }
    return {
        templateUrl : vPath.VIEWS + "ui-list-tabs.html",
        transclude: true,
        scope : {
            addClass: "@addClass",
            width: "@width",
            list: "=list"
        },
        controller: function($scope, $element, $attrs){
            $scope.listTabs = [];
            this.addTab = function(scope){
                $scope.listTabs.push(scope);
            };
        },
        require : "^?uiForm",
        link : function(scope, element, attrs, ctrl) {
            if(!scope.width){scope.width = 12;}
            scope.clickTab = function(index){
                scope.indexSelected = index;
                setPositionTab(scope, element);
               
            };
            
            scope.prevTab = function(){
                var tabBackground = element.find(".tabs-background");
                var backgroundLeft = parseInt(tabBackground.css("left").replace("px", ""));
                var prevLeft = backgroundLeft + 315;
                if(prevLeft > 0){
                    prevLeft = 0;
                }
                console.log("NexLeft: " + prevLeft);
                console.log("backgroundLeft: " + backgroundLeft);
                tabBackground.css("left",  prevLeft);
                $timeout(function(){setPositionTab(scope, element);},400);
            };
            scope.nextTab = function(){
                var tabBackground = element.find(".tabs-background");
                var maxLeft = getMaxLeft(element);
                var backgroundLeft = parseInt(tabBackground.css("left").replace("px", ""));
                var nextLeft = backgroundLeft - 315;
                if(nextLeft < maxLeft){
                    nextLeft = maxLeft;
                }
                tabBackground.css("left",  nextLeft);
                $timeout(function(){setPositionTab(scope, element);},400);
                
            };
            
            scope.endTabs = function(){
                element.find(".tabs-body").css("width", 100/scope.listTabs.length + "%");
                element.find(".border-tabs").delay(300).css("display", "");
                element.find(".tabs-background").css("left", "0px");
                $timeout(function(){setPositionTab(scope, element);}, 500);
            };
            
            angular.element($window).bind('resize', function() {
                $timeout(function(){setPositionTab(scope, element);}, 1500);
            });
            $timeout(function(){$.material.ripples(element.find(".tabs, .tab-prev, .tab-next"));}, 1000);
        }
    };
});

moduleDirective.directive("uiTab", function(utilListener, vPath, $timeout) {
    return {
        templateUrl : vPath.VIEWS + "ui-tab.html",
        transclude: true,
        scope : {
            addClass : "@addClass",
            label: "@label",
            icon: "@icon",
            page: "@page"
        },
        require : "^?uiListTabs",
        link : function(scope, element, attrs, ctrl) {
            if(scope.page){scope.pageNotLoad = true;}
            if(ctrl){
                ctrl.addTab(scope);
            }
        }
    };
});

moduleDirective.directive("uiSnackBar", function(utilValid, vPath) {
    return {
            templateUrl : vPath.VIEWS + "ui-snack-bar.html",
            replace : true,
            scope : {
                    width : "@width",
                    addClass : "@addClass",
                    timeOutSnack : "@timeOutSnack",
                    uiMensagem : "@uiMensagem",
                    tipoClass : "@tipoClass",
                    labelSnack : "@labelSnack",
                    position : "@position",
                    value : "=value"
            },
            link : function(scope, element) {
                    $.material.ripples(element.find("ui-snack-bar"));
                    utilValid.validSnackBar(scope);
            }
    };
});

angular.module("uiDirective").directive("uiMenuNotification", function(vPath) {
    return {
            templateUrl : vPath.VIEWS + "ui-menu-notification.html",
            replace : true,
            transclude : true,
            scope : {
                icon : "@icon",
                label : "@label",
                addClass : "@addClass",
                badges : "=badges",
                notifications : "=notifications"
            },
            link : function(scope, element, attrs) {
                var btn = element.find(".btn");
                $.material.ripples(btn);
            }
    };
});

angular.module("uiDirective").directive("uiFooter", function(vPath) {
    return {
        templateUrl : vPath.VIEWS + "ui-footer.html",
        transclude : true
    };
});