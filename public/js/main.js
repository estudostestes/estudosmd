var appPrincipal = angular.module("pilotoFidelidade",
["ngAnimate","utilService", "utilValue", "uiDirective", "ngRoute"]); 

appPrincipal.config(function($routeProvider, $locationProvider, $httpProvider) {
    
    
    $locationProvider.hashPrefix('');
//    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('tokenInterceptor');
    
    $routeProvider.when('/dashboard', {
        templateUrl: 'paginas/dashboard.html',
        controller: 'DashboardCTRL'
    });
    $routeProvider.when('/tabs', {
        templateUrl: 'paginas/tabs.html',
        controller: 'DashboardCTRL'
    });
//    $routeProvider.otherwise({redirectTo: '/index'});

});