
//Módulo de serviços
var moduleService = angular.module("utilService",[]);
// Util funções de requisições HTTP -----------------------------------------------------------------------------------------------
moduleService.factory("utilHttp", function($http, utilFunc){
    var utilHttp = {};
    utilHttp.addTimestamp = function(url){
        var timestamp = new Date().getTime();
        if(url.indexOf("?") > 0){
            url = url + "&timestamp=" + timestamp;
        }else{
            url = url + "?timestamp=" + timestamp;			
        }
        console.log(url);
        return url;
    };
	
    utilHttp.ajax = function(type, url, obj, funcSuccess, funcError){
        url = _addTimestamp(url);
        if(!funcSuccess){
            funcSuccess = function(result){
                utilFunc.getMsgResult(result.data);
            };
        };
        if(!funcError){
            funcError = function(result){
                utilFunc.getMsgStatusError(result.status);
            };
        }
        $http({
          method: type,
          url: url,
          data: obj
        }).then(funcSuccess, funcError);
    };
    
    return utilHttp;
});

//------------------------ Util funções de Diálogos na tela -----------------------------------------------------------------------------------------------
moduleService.factory("utilDialog", function($compile, $window, $document, $timeout){
    var utilDialog = {};
    utilDialog.setDatePicker = function(scope, element, format){
        if(!format){format = "dd/mm/yyyy";};
        var input = element.find("input");
        input.val(scope.value);
        input.datepicker({
            format: format,
            autoclose: true,
            todayHighlight: true,
            forceParse: false
        });
    };
	
    utilDialog.setClockPicker = function(scope, element){
        var input = element.find("input");
        input.val(scope.value);
        input.clockpicker({
            placement: "top",
            align: 'left',
            donetext: 'button',
            autoclose: 'true',
            vibrate: 'true',
            todayBtn: 'true'
        });
    };
	
    utilDialog.getTypeDialog = function(tipo){
        switch(tipo){
            case "padrao": tipo = 'type-default';
            break;
            case "informacao": tipo = 'type-info';
            break;
            case "primario": tipo = 'type-primary';
            break;
            case "sucesso": tipo = 'type-success';
            break;
            case "atencao": tipo = 'type-warning';
            break;
            case "erro": tipo = 'type-danger';
            break;
            default: tipo = 'type-default';
        }
        return tipo;
    };
	
    utilDialog.showMsg = function(msg, titulo, tipo, largura){
        if(!titulo){titulo = "Mensagem";}
        if(!tipo){tipo = 'type-default';}
        else{tipo = _getTypeDialog(tipo);}
        if(!largura){largura = '';}
        var modal = new BootstrapDialog({
            title: titulo,
            message: msg,
            type: tipo,
            width: largura,
            draggable: true,
            autodestroy: true,
            buttons: [{
                icon: 'check',       
                label: 'OK',
                cssClass: 'btn-default', 
                autospin: false,
                action: function(dialog){    
                    dialog.close();
                }
            }]
        });
        modal.realize();
        modal.open();
        return modal;
    };
	
    utilDialog.showModal = function(scope, msg, titulo, botoes, largura, funcaoMostrar, funcaoEsconder, podeFechar, paginaHTML, tipo){
        if(!msg){msg = "Aguarde...";}
        if(!titulo){titulo = "Aguarde...";}
        if(!botoes){botoes = [];}
        if(!largura){largura = '';}
        if(!funcaoMostrar){funcaoMostrar = function(){};}
        if(!funcaoEsconder){funcaoEsconder = function(){};}
        if(paginaHTML){
            msg = function(dialog) {
                var $message = $('<div></div>');
                var pageToLoad = dialog.getData("pageToLoad");
                $message.load(pageToLoad,null, function(){
                    $message = $compile($message)(scope);
                });
                return $message;
            };
        }
        if(!tipo){
                tipo = 'type-default';
        }else{
                tipo = _getTypeDialog(tipo);
        }
        var modal = new  BootstrapDialog({
            title: titulo,
            message: msg,
            type: tipo,
            width: largura,
            buttons: botoes,
            onshow: funcaoMostrar,
            onhide: funcaoEsconder,
            draggable: true,
            closable:podeFechar,
            tirarBR: true,
            data: {
                "pageToLoad":paginaHTML
            }
         });
        modal.realize();
        modal.open();
        return modal;
    };
	
    utilDialog.showConfirm = function(msg, titulo, tipo, funcao, cancelLabel, okLabel){
        BootstrapDialog.confirm(msg, titulo, tipo, funcao, cancelLabel, okLabel);
    };
	
    utilDialog.openLoading = function(scope, msg, size){
        if(!angular.element(document.querySelectorAll(".loading-screen-black")).html()){
            $(document.body).toggleClass('modal-open', true);
            if(!msg){msg = "Aguarde...";}
            if(!size){size = "";}
            var html = '<div class="loading-screen-black">'
                +'<ui-loading-circle label="' + msg + '" size="' + size
                + '" style="margin-top: 5em;"> </ui-loading-circle>'
                              + '</div>';
             var html = $compile(html)(scope);
             console.log(html);
             $("body").append(html);
        }
    };
	
    utilDialog.closeLoading = function(){
        angular.element(document.querySelectorAll(".loading-screen-black")).fadeOut(500,function(){
            this.remove();
            if($.isEmptyObject(BootstrapDialog.dialogs)){
                $(document.body).toggleClass('modal-open', false);
            }
        });
    };
	
   utilDialog.setMsgLoading = function(msg){
        var screenLoading = angular.element(document.querySelectorAll(".loading-screen-black")).find(".spinner-label");
        screenLoading.fadeOut(300, function(){
            screenLoading.html(msg).delay(100).fadeIn(300);
        });
    };
	
    utilDialog.createButton = function(button){
        var dialog = new BootstrapDialog();
        return  dialog.createButton(button); 
    };
	
    utilDialog.createButtonDialog = function(texto, icone, classe, funcao){
        if(!texto){texto = "";};
        if(!icone){icone = "";};
        if(!classe){classe = "";};
        if(!funcao){funcao = function(dialog){};};

        var botao = {
            icon: icone,
            label: texto,
            cssClass: classe,
            action:funcao
        };
        return botao;
    };
	
    utilDialog.showSlideRight = function(scope, titulo, widthPercent, page){
        if(!widthPercent){
            widthPercent = 50;
        }
        var navSlide = $document.find(".nav-slide-right");
        var titleLabel = $document.find("#nav-slide-title-label");
        var load = '<div style="margin-top: 3em;"><ui-loading-circle size="sm"></ui-loading-circle></div>';
        $document.find(".nav-slide-right-container").css({"min-width" : widthPercent + "%", "max-width": widthPercent + "%"});
        navSlide.css({"min-width" : widthPercent + "%", "max-width": widthPercent + "%"});
        navSlide.css("right", 0 + "%");
        if(titleLabel.html() !== titulo){
            titleLabel.fadeOut(500, function(){
                titleLabel.html(titulo).delay(100).fadeIn(400,function(){
                });
            });
        }
        load = $compile(load)(scope);
        $document.find(".nav-slide-right-container").html(load);
        if(page){
            var pageHTML = $('<div></div>');
            pageHTML.load(page,null, function(){
                pageHTML = $compile(pageHTML)(scope);
                $document.find(".nav-slide-right-container").html(pageHTML);
            });
        }
    };
	
    utilDialog.closeSlideRight = function(){
            var widthPercent = parseInt($document.find(".nav-slide-right").css("width")); 
            var widthNegative = (widthPercent * -1);
            $document.find(".nav-slide-right").css("right", (widthNegative - 20));
    };
	
    utilDialog.preLoad = function(scope, msg, time, size){
        this.openLoading(scope, msg, size);
        var close = this.closeLoading;
        if(!time){time = 500;};
        angular.element(document).ready(function () {
            $timeout(function(){
                close();
            },time);
        });
    };
    return utilDialog;
});

//------------------------ Util funções de mascaras -------------------------------------------------------------------------------------------------------
moduleService.factory("utilMask", function(utilFunc){
    var utilMask = {};
    utilMask.maskNumber = function(scope){
        scope.value = utilFunc.getOnlyNumbers( scope.value);
    };
	
    utilMask.maskMax = function(scope){
        if(scope.value){
            scope.showErro = false;
            if(scope.value.length > scope.maskMax){
                scope.value = scope.value.substring(0, scope.maskMax);
                if(scope.countMax){
                    scope.msgInput = scope.maskMax - scope.value.length;
                }
            }else if(scope.countMax){
                scope.showErro = true;
                scope.msgInput = scope.maskMax - scope.value.length;
            }
        }

    };
	
    utilMask.maskDate = function(scope){
        if(scope.value){
            var data = utilFunc.getOnlyNumbers( scope.value);
            if(data.length > 2){
                    data = data.substring(0, 2) + "/" + data.substring(2);
            }
            if(data.length > 5){
                    data = data.substring(0, 5) + "/" + data.substring(5, 9);
            }
            scope.value = data;
        }
    };
	
    utilMask.maskClock = function(scope){
        if(scope.value){
            var hora = utilFunc.getOnlyNumbers( scope.value);
            if(hora.length > 2){
                    hora = hora.substring(0, 2) + ":" + hora.substring(2,4);
            }
            scope.value = hora;
        }
    };

    utilMask.maskCpf = function(scope){
        if(scope.value){
            var cpf = utilFunc.getOnlyNumbers( scope.value);
            if(cpf.length > 3){
                cpf = cpf.substring(0, 3) + "." + cpf.substring(3);
            }
            if(cpf.length > 7){
                cpf = cpf.substring(0, 7) + "." + cpf.substring(7);
            }
            if(cpf.length > 11){
                cpf = cpf.substring(0,11) + "-" + cpf.substring(11, 13);
            }
            scope.value = cpf;
        }
    };
	
    utilMask.maskCnpj = function(scope){
        if(scope.value){
            var cnpj = utilFunc.getOnlyNumbers( scope.value);
            if(cnpj.length > 2){
                cnpj = cnpj.substring(0, 2) + "." + cnpj.substring(2);
            }
            if(cnpj.length > 6){
                cnpj = cnpj.substring(0, 6) + "." + cnpj.substring(6);
            }
            if(cnpj.length > 10){
                cnpj = cnpj.substring(0, 10) + "/" + cnpj.substring(10);
            }
            if(cnpj.length > 15){
                cnpj = cnpj.substring(0, 15) + "-" + cnpj.substring(15, 17);
            }
            scope.value = cnpj;
        }
    };
	
    utilMask.maskCep = function(scope){
        if(scope.value){
            var cnpj = utilFunc.getOnlyNumbers( scope.value);
            if(cnpj.length > 5){
                cnpj = cnpj.substring(0, 5) + "-" + cnpj.substring(5, 8);
            }
            scope.value = cnpj;
        } 
    };
    return utilMask;
});

//------------------------------ Util funções Gerais -------------------------------------------------------------------------------------------------------
moduleService.factory("utilFunc", function(utilDialog, vStatus, vMsg, $window, $document, $timeout){
    var utilFunc = {};
    utilFunc.getOnlyNumbers = function(value){
        if(value){
            try {
                return value.replace(/[^0-9]+/g, "");
            } catch (e) {console.log(e);}
        }
    };
	
    utilFunc.getMsgStatusError = function (status){
        var msg = vStatus.statusDefault;
        switch (status) {
            case 302: msg = vStatus.S302; break;
            case 304: msg = vStatus.S304; break;
            case 400: msg = vStatus.S400; break;
            case 401: msg = vStatus.S401; break;
            case 403: msg = vStatus.S403; break;
            case 404: msg = vStatus.S404; break;
            case 405: msg = vStatus.S405; break;
            case 410: msg = vStatus.S410; break;
            case 500: msg = vStatus.S500; break;
            case 502: msg = vStatus.S302; break;
            case 503: msg = vStatus.S302; break;
            default: break;
        }
        utilDialog.closeLoading();
        utilDialog.showMsg(msg, "Erro!", "erro");
    };
	
    utilFunc.getMsgResult = function(result){
        var msg = null;
        var tipo = "";
        if(result.msgError){tipo = "erro";msg = result.msgError;}
        else if(result.msgInformacao){tipo = "informacao";msg = result.msgInformacao;}
        else if(result.msgSucesso){tipo = "sucesso";msg = result.msgSucesso;}
        else if(result.msgAtencao){tipo = "atencao";msg = result.msgAtencao;}
        if(msg){utilDialog.closeLoading();utilDialog.showMsg(msg, null, tipo);}
    };
	
	
    utilFunc.getItemList = function(id, lista){
        var item = null;
        if(id && !$.isEmptyObject(lista)){
            for(var index = 0; index < lista.length; index++){
                if(lista[index].id === id){
                    item = lista[index];
                    break;
                }
            }
        }
        return item;
    };
	
    utilFunc.removeItemList = function(id, lista){
        var item = null;
        if(id && !$.isEmptyObject(lista)){
            for(var index = 0; index < lista.length; index++){
                if(lista[index].id === id){
                    lista.splice(index, 1);
                    break;
                }
            }
        }
    };
	
    utilFunc.setPositionSelect = function(element){
        var heightfiltro = parseInt(element.find(".lista-select").css("height").replace("px", ""));
        var height = parseInt(element.find(".resultado-lista-select").css("height").replace("px", ""));
        var w = angular.element($window);
        var offSet = element.offset().top;
        var scrollTop = w.scrollTop();
        var height = element.find(".lista-select").css("height").replace("px", "");
        var heightInput = parseInt(element.find(".form-control:first").css("height").replace("px", ""));
        var center = (w.height()/2);
        var valor =  (heightInput -  (heightInput/2)) * -1;
        if(((offSet + heightInput) - scrollTop) >= center && offSet > height){
                valor = ((heightfiltro - height)/3) * -1;
                element.find(".lista-select").css("top", "auto");
                element.find(".lista-select").css("bottom", valor);
        }else{
                element.find(".lista-select").css("bottom", "auto");
                element.find(".lista-select").css("top", valor);
        }
    };
	
    utilFunc.setTableFixed = function(element){
        angular.element($window).bind("scroll", function() {
            var topTable = element.offset().top;
            var widthTable = element.find("#firstHead:first").width();
            var leftTable = element.find("#firstHead:first").offset().left;
            var tableFixed = element.find(".table-fixed");
            var top = 0;
            if (this.pageYOffset >= topTable) {
                if($document.find(".nav-fixed").html()){
                    top = $document.find(".nav-fixed").height();
                }
                tableFixed.css({"min-width": "100%", "top": top, "left": 0, "right": 0});
                tableFixed.find(".table").css({"width": widthTable, "margin-left": leftTable, "margin-bottom": 0});
                tableFixed.fadeIn(300);
            } else {
                tableFixed.fadeOut(300);
            }
        });
    };
    
    utilFunc.configLoading = function(scope, element) {
        $timeout(function() {
            var lgText = 0;
            if (scope.label) {
                lgText = parseInt(element.find(".spinner-label").css("width"));
            }
            var lgSpinner = parseInt(element.find(".spinner").css("width"));
            var lgContainer = parseInt(element.css("width"));
            var position = (lgContainer / 2) - ((lgText + lgSpinner) / 2);
            position = (position * 100) / lgContainer;
            element.find(".container-spinner").css({
                left : Math.round(position) + "%",
                opacity : 1,
                width : (90 - position) + "%"
            });
        }, 200);
    };
	
    utilFunc.loadPage = function(page, scope){
        var $message = $('<div></div>');
     $message.load(page,null, function(){
        $message = $compile($message)(scope);
     });
     return $message;
    };
	
    utilFunc.setFixeNav = function(element, isMin){
        $timeout(function(){
            var heightNav = element.find(".navbar").height();
            var navFaker = element.find(".nav-faker");
            var navFixed = element.find(".nav-not-faker");
            navFixed.addClass("nav-fixed");
            if(isMin){
                    navFaker.addClass("nav-min");
                    navFixed.addClass("nav-min");
            }else{
                    navFaker.removeClass("nav-min");
                    navFixed.removeClass("nav-min");
            }
            $timeout(function(){
                    heightNav = element.find(".navbar").height();
                    console.log("height: " + heightNav);
                    navFaker.css({height: (heightNav *1.47), display: "block"});
            },300);
        },300);
    };
    return utilFunc;
});

//----------------------------- Util para validações -------------------------------------------------------------------------------------------------------
moduleService.factory("utilValid", function(vMsg, utilMask, utilFunc, utilDialog, $timeout){
    var utilValid = {};
    utilValid.validScopes = function(scopes){
        var msg = "";
        var quebraLinha = "\n";
        scopes.forEach(function(scope) {
            var campo = "";
            if(scope.label){
                campo = scope.label;
            }else if(scope.placeholder && !scope.notPlaceholder){
                campo = scope.placeholder;
            }else if(scope.name){
                campo = scope.name;
            }
            if(scope.isSelect){
                if(scope.notEmpty && $.isEmptyObject(scope.value) && $.isEmptyObject(scope.multValue)){
                    msg += vMsg.O_CAMPO + campo + " " + vMsg.E_OBRIGATORIO + quebraLinha;
                }
            }else if(scope.notEmpty && !scope.value){
                msg += vMsg.O_CAMPO + campo + " " + vMsg.E_OBRIGATORIO + quebraLinha;
            }else if(scope.validMin && scope.showErro){
                msg += vMsg.O_CAMPO + campo + " " + vMsg.TEM_TER_NO_MINIMO + " "  + scope.validMin + " " + vMsg.CARACTERES  +quebraLinha;
            }else if(scope.validDate && scope.showErro){
                msg += vMsg.A_DATA + " " + vMsg.INFORMADA_CAMPO + campo + " " + vMsg.E_INVALIDA + quebraLinha;
            }else if(scope.validClock && scope.showErro){
                msg += vMsg.A_HORA + " " + vMsg.INFORMADA_CAMPO +  " " + campo + " " + vMsg.E_INVALIDA +  quebraLinha;
            }else if(scope.validCpf && scope.showErro){
                msg += vMsg.O_CPF + " " + vMsg.INFORMADO_CAMPO +  " " + campo + " " + vMsg.E_INVALIDO +  quebraLinha;
            }else if(scope.validCnpj && scope.showErro){
                msg += vMsg.O_CNPJ + " " + vMsg.INFORMADO_CAMPO +  " " + campo + " " + vMsg.E_INVALIDO +  quebraLinha;
            }else if(scope.validCep && scope.showErro){
                msg += vMsg.O_CEP + " " + vMsg.INFORMADO_CAMPO +  " " + campo + " " + vMsg.E_INVALIDO +  quebraLinha;
            }else if(scope.validEmail && scope.showErro){
                msg += vMsg.O_EMAIL + " " + vMsg.INFORMADO_CAMPO +  " " + campo + " " + vMsg.E_INVALIDO +  quebraLinha;
            }else if(scope.validNumber && scope.showErro){
                msg += vMsg.NO_CAMPO + " " + campo + " " + vMsg.APENAS_NUMERO.toLowerCase() + quebraLinha;
            }else if(scope.compare && scope.showErro){
                msg += vMsg.O_CAMPO + " " + campo + " " + vMsg.DEVE_SER_IGUAL.toLowerCase() + scope.compareLabel + quebraLinha;
            }
        });
        utilDialog.showMsg(msg, vMsg.VALIDACAO, "erro");
    };
	
    utilValid.validNumber = function(scope){
        if(scope.value.length >= 1){
            var notNumber = /[^0-9]+/g;
            notValid = notNumber.test(scope.value);
            scope.msgInput = vMsg.APENAS_NUMERO;
            scope.showErro = notValid;
        }
    };
	
    utilValid.validMin = function(scope){
            if(scope.value && scope.value.length < scope.validMin){
                    scope.msgInput = vMsg.MINIMO + scope.validMin;
                    scope.showErro = true;
            }else{
                    scope.showErro = false;
            }
    }
	
    utilValid.validCep = function(scope){
        if(scope.value){
            var cep = /^\d{5}\-?\d{3}$/;
            notValid = !cep.test(scope.value);
            scope.msgInput = vMsg.CEP_INVALIDO;
            scope.showErro = notValid;
        }
    };
	
    utilValid.validNumber = function(scope){
        if(scope.value.length >= 1){
            var notNumber = /[^0-9]+/g;
            notValid = notNumber.test(scope.value);
            scope.msgInput = vMsg.APENAS_NUMERO;
            scope.showErro = notValid;
        }
    };
	
    utilValid.validEmptyInputFloatMD = function(scope){
        if (!scope.value) {
            scope.empty = "is-empty";
        }else {
            scope.empty = "";
        }
    };
	
    utilValid.validValueEmptyInputFloat = function(scope, element, focus){
        var focus = element.find(".is-focused").html();
        if(focus && !scope.value){
            scope.msgInput = vMsg.CAMPO_OBRIGATORIO;
            scope.showErro = true;
        }else{
            scope.showErro = false;
        }
    };
	
    utilValid.validDate = function(scope){
        var data = scope.value;
        var notValid = false;
        if (data && data.length === 10) {
            var dia = data.substring(0, 2);
            var mes = data.substring(3, 5);
            var ano = data.substring(6, 10);
            // Criando um objeto Date usando os valores ano, mes e dia.
            var novaData = new Date(ano, (mes - 1), dia);
            var mesmoDia = parseInt(dia, 10) == parseInt(novaData.getDate());
            var mesmoMes = parseInt(mes, 10) == parseInt(novaData.getMonth()) + 1;
            var mesmoAno = parseInt(ano) == parseInt(novaData.getFullYear());

            if (!((mesmoDia) && (mesmoMes) && (mesmoAno))) {
                notValid = true;
            }
        }else{
                notValid = true;
        }
        scope.msgInput = vMsg.DATA_INVALIDA;
        scope.showErro = notValid;
    };
	
    utilValid.validClock = function(scope){
        var hora = scope.value;
        var notValid = false;
        if (hora && hora.length == 5) {
            var horas = hora.substring(0, 2);
            var minutos = hora.substring(3, 5);
            // Criando um objeto Date usando os valores ano, mes e dia.
            if(horas){
                horas =  parseInt(horas);
                if(horas > 23){
                    notValid = true;
                }
            }
            if(minutos){
                minutos =  parseInt(minutos);
                if(minutos > 59){
                    notValid = true;
                }
            }
        }else{
            notValid = true;
        }
        scope.msgInput = vMsg.HORA_INVALIDA;
        scope.showErro = notValid;
    }
	
    utilValid.validCpf = function(scope){
        var notValid = false;
        CPF = scope.value;
        error  = new String;
        cpfv  = CPF;
        if(cpfv.length == 14 || cpfv.length == 11){
            cpfv = cpfv.replace('.', '');
            cpfv = cpfv.replace('.', '');
            cpfv = cpfv.replace('-', '');

            var nonNumbers = /\D/;

            if(nonNumbers.test(cpfv)){
                    error = vMsg.APENAS_NUMERO;
                    notValid = true;

            }else{
                if (cpfv == "00000000000" ||
                    cpfv == "11111111111" ||
                    cpfv == "22222222222" ||
                    cpfv == "33333333333" ||
                    cpfv == "44444444444" ||
                    cpfv == "55555555555" ||
                    cpfv == "66666666666" ||
                    cpfv == "77777777777" ||
                    cpfv == "88888888888" ||
                    cpfv == "99999999999") {
                    error = vMsg.CPF_INVALIDO;
                    notValid = true;
                }
                var a = [];
                var b = new Number;
                var c = 11;
                for(i=0; i<11; i++){
                    a[i] = cpfv.charAt(i);
                    if (i < 9) b += (a[i] * --c);
                }
                if((x = b % 11) < 2){
                    a[9] = 0;
                }else{
                    a[9] = 11-x;
                }
                b = 0;
                c = 11;
                for (y=0; y<10; y++) b += (a[y] * c--);
                if((x = b % 11) < 2){
                    a[10] = 0;
                }else{
                    a[10] = 11-x;
                }
                if((cpfv.charAt(9) != a[9]) || (cpfv.charAt(10) != a[10])){
                    error = vMsg.CPF_INVALIDO;
                    notValid = true;
                }else{
                    notValid = false;
                }
            }
        }else {
            error = vMsg.CPF_INVALIDO;
            notValid = true;
        }
        scope.msgInput = error;
        scope.showErro = notValid;
    };
	

	
    utilValid.validCnpj = function(scope, element){
        var notValid = false;
        cnpj = utilFunc.getOnlyNumbers( scope.value);
        if (cnpj.length != 14){
                notValid = true;
        }else if (cnpj == "00000000000000" ||//Elimina CNPJs invalidos conhecidos 
            cnpj == "11111111111111" || 
            cnpj == "22222222222222" || 
            cnpj == "33333333333333" || 
            cnpj == "44444444444444" || 
            cnpj == "55555555555555" || 
            cnpj == "66666666666666" || 
            cnpj == "77777777777777" || 
            cnpj == "88888888888888" || 
            cnpj == "99999999999999"){
                notValid = true; 
            }else{
                tamanho = cnpj.length - 2;
                numeros = cnpj.substring(0,tamanho);
                digitos = cnpj.substring(tamanho);
                soma = 0;
                pos = tamanho - 7;
                for (i = tamanho; i >= 1; i--) {
                    soma += numeros.charAt(tamanho - i) * pos--;
                    if (pos < 2)
                        pos = 9;
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(0)){
                    notValid = true;
                }else{
                    tamanho = tamanho + 1;
                    numeros = cnpj.substring(0,tamanho);
                    soma = 0;
                    pos = tamanho - 7;
                    for (i = tamanho; i >= 1; i--) {
                        soma += numeros.charAt(tamanho - i) * pos--;
                        if (pos < 2)
                            pos = 9;
                    }
                    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                    if (resultado != digitos.charAt(1)){
                            notValid = true;				
                    }
                }
            }
        scope.msgInput = vMsg.CNPJ_INVALIDO;
        scope.showErro = notValid;
    };
	
    utilValid.validEmail = function(scope){
        var email = scope.value;
        var validador = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var notValid = !validador.test(email);
        scope.msgInput = vMsg.EMAIL_INVALIDO;
        scope.showErro = notValid;
    };
	
    utilValid.validCompare = function(scope){
        var notValid = false;
        if(scope.value && scope.value != scope.compare){
            notValid = true;
        }
        scope.msgInput = vMsg.DEVE_SER_IGUAL+ scope.compareLabel;
        scope.showErro = notValid;
    };
	
    utilValid.validSelect = function(scope, element){
        scope.msgInput = vMsg.CAMPO_OBRIGATORIO;
        if(scope.notEmpty){
            if($.isEmptyObject(scope.value) && $.isEmptyObject(scope.multValue)){
                    scope.showErro = true;
            }else{
                    scope.showErro = false;
            }
        }
    };
	
    utilValid.validClickSelect = function(scope, element, item){
        if(scope.multValue){
            var itemLista = utilFunc.getItemList(item.id, scope.multValue);
            if(itemLista){
                    utilFunc.removeItemList(item.id, scope.multValue);
            }else{
                    scope.multValue.push(item);
            }
        }else if(scope.value){
            if(item && scope.value.id != item.id){
                if(!$.isEmptyObject(scope.value)){
                    scope.idBefore = scope.value.id;
                    scope.value = {};
                    utilValid.validSetSelect(scope, element);
                }
                scope.idBefore = item.id;
                scope.value = item;
            }else if(!scope.notDiselected){
                scope.idBefore = item.id;
                scope.value = {};
                utilValid.validSetSelect(scope, element);
            }else{
                utilValid.validSetSelect(scope, element);
            }
        }
    };
	
    utilValid.validSetSelect = function(scope, element, notValid){
        if(scope.multValue){
            var text = "";
            var lista = scope.multValue;
            element.find(".resultado-lista-select li").css("font-weight", "normal");
            element.find(".resultado-lista-select li i").css("display", "none");
            lista.forEach(function(item){
                element.find("#liSelect" + item.id).css("font-weight", "600");
                element.find("#liSelect" + item.id + " i").css("display", "block");
                if(text){
                    text += ", ";
                }
                text += item[scope.field];
            });
            element.find(".form-control:first").val(text);
        }else{
            if($.isEmptyObject(scope.value)){
                element.find("#liSelect" + scope.idBefore).css("font-weight", "normal");
                element.find("#liSelect" + scope.idBefore + " i").css("display", "none");
                element.find(".form-control:first").val("");
            }else{
                if(scope.idBefore){
                    element.find("#liSelect" + scope.idBefore).css("font-weight", "normal");
                    element.find("#liSelect" + scope.idBefore + " i").css("display", "none");
                    element.find(".form-control:first").val("");
                }
                element.find("#liSelect" + scope.value["id"]).css("font-weight", "600");
                element.find("#liSelect" + scope.value["id"] + " i").css("display", "block");
                element.find(".form-control:first").val(scope.value[scope.field]);
            }
            if(!notValid){
                element.find("#select" + scope.$id).prop("checked" , false);
            }
        }
        if(!notValid){
                utilValid.validSelect(scope, element);
        }
    };
    return utilValid;
});
//---------------------------- Util funções de ouvintes --------------------------------------------------------------------------------------------------
moduleService.factory("utilListener", function(utilMask, utilValid, utilDialog, utilFunc, $timeout){
    var utilListener = {};
    utilListener.setListenerInputFloat = function(scope, element){
        if(scope.datePicker){
            utilDialog.setDatePicker(scope, element);
        }else if(scope.clockPicker){
            utilDialog.setClockPicker(scope, element);
        }
        scope.$watch('value', function() {
            utilValid.validEmptyInputFloatMD(scope);
            if(scope.notEmpty){
                utilValid.validValueEmptyInputFloat(scope, element);
            }
            //Macaras
            if(scope.maskDate){
                utilMask.maskDate(scope);
            }else if(scope.maskClock){
                utilMask.maskClock(scope);
            }else if(scope.maskCpf){
                utilMask.maskCpf(scope);
            }else if(scope.maskCnpj){
                utilMask.maskCnpj(scope);
            }else if(scope.maskCep){
                utilMask.maskCep(scope);
            }else if(scope.maskNumber){
                utilMask.maskNumber(scope);
            }else if(scope.maskMax){
                utilMask.maskMax(scope);
            }
            //Validações
            if(scope.value && scope.value.length == 10 && scope.validDate){
                utilValid.validDate(scope);
            }else if(scope.value && scope.value.length == 5 && scope.validClock){
                utilValid.validClock(scope);
            }else if(scope.value && scope.value.length == 14 && scope.validCpf){
                utilValid.validCpf(scope);
            }else if(scope.value && scope.value.length == 18 && scope.validCnpj){
                utilValid.validCnpj(scope);
            }else if(scope.value && scope.validNumber){
                utilValid.validNumber(scope);
            }else if(scope.compare){
                utilValid.validCompare(scope);
            }
        });
    };
	
    utilListener.setListenerToggle = function(scope, element){
        scope.$watch('value', function() {
            if(scope.flagIni){//Flag para não chamar a função na inicialização.
                if(scope.onChange){
                    scope.onChange(scope.value);
                }
            }
            scope.label = scope.value ? scope.labelTrue : scope.labelFalse ? scope.labelFalse : scope.labelTrue;//Se o labelFalse não for informado, assume o labelTrue
        });
    };
	
    utilListener.setCheckbox = function(scope, element){
        if(scope.value && scope.selecteds){
            var itemLista = utilFunc.getItemList(scope.value.id, scope.selecteds);
            if(itemLista && scope.checked && scope.init){
                utilFunc.removeItemList(scope.value.id, scope.selecteds);
                scope.checked = false;
            }else if(itemLista && !scope.checked){
                scope.checked = true;
            }else if(!itemLista && scope.checked){
                scope.selecteds.push(scope.value);
                scope.checked = true;
            }else if(itemLista && scope.checked && !scope.init){
                scope.checked = true;
            }
        }
    };
	
    utilListener.setListenerSelect = function(scope,element){
        if(scope.value){
            scope.$watch('value', function() {
                if($.isEmptyObject(scope.value)){
                    scope.empty = "is-empty";
                }else{
                    scope.empty = "";
                    utilValid.validSetSelect(scope, element);
                }
            });
        }
        if(scope.multValue){
            scope.$watch('multValue', function() {
                if($.isEmptyObject(scope.multValue) || !scope.multValue){
                        scope.empty = "is-empty";
                }else{
                        scope.empty = "";
                }
                if(!scope.isFirstLoad){
                        utilValid.validSetSelect(scope, element);
                }
                scope.isFirstLoad = false;
            }, true);
        }
    };
    return utilListener;
});
//------------------------------- Util funções de eventos dos elementos --------------------------------------------------------------------------------------------
moduleService.factory("utilEvent", function(utilValid, utilDialog){
    var utilEvent = {};
    utilEvent.onBlurInputFloat = function(scope, element){
        if(scope.value){
            if(scope.validDate){
                utilValid.validDate(scope);
            }else if(scope.validClock){
                utilValid.validClock(scope);
            }else if(scope.validCpf){
                utilValid.validCpf(scope);
            }else if(scope.validCnpj){
                utilValid.validCnpj(scope);
            }else if(scope.validEmail){
                utilValid.validEmail(scope);
            }else if(scope.validNumber){
                utilValid.validNumber(scope);
            }else if(scope.validCep){
                utilValid.validCep(scope);				
            }else if(scope.validMin){
                utilValid.validMin(scope);
            }
        }
    };

    utilEvent.onFocusInputFloat = function(scope, element){
//		if(scope.clockPicker){
//			utilDialog.setClockPicker(scope, element);
//		}
    };
    return utilEvent;
});
//------------------------------- Interceptador de requisições --------------------------------------------------------------------------------------------
moduleService.factory('tokenInterceptor', function($q, $window, $location) {
        var interceptor = {};
//        interceptor.request = function(config) {
//            // enviar o token na requisição
//            config.headers = config.headers || {};
//            if ($window.sessionStorage.token) {
//                console.log('Enviando token já obtido em cada requisição');
//                config.headers['x-access-token'] = $window.sessionStorage.token;
//            }
//            return config;
//        };
//        interceptor.response = function (response) {
//            var token = response.headers('x-access-token');
//            if (token != null) {
//                $window.sessionStorage.token = token;
//                console.log('Token no session storage: ', token);
//            } 
//            return response;
//        };

//        interceptor.responseError = function(rejection) {
//            if (rejection != null && rejection.status === 401) {
//                console.log('Removendo token da sessão');
//                delete $window.sessionStorage.token;
//                $location.path("/login");
//            } 
//            return $q.reject(rejection);
//        };
    return interceptor;
});
