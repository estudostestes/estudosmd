var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var path = require('path');

var app = express();

app.set('secret', 'homemavestruz'); 
app.use(express.static('./public'));
app.use(bodyParser.json());

consign({cwd: 'app'})
    .include('model')
    .then('api')
    .then('route/auth.js')
    .then('route')
    .into(app);

module.exports = app;
